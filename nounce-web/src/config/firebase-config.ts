import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';
import { getDatabase } from 'firebase/database';
import { getStorage } from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyCPqkTMZfKkNB0dgI-HhtSntoullBx8MrM",
  authDomain: "nounce-e4f5c.firebaseapp.com",
  projectId: "nounce-e4f5c",
  storageBucket: "nounce-e4f5c.appspot.com",
  messagingSenderId: "956819736986",
  appId: "1:956819736986:web:4d7bf2b9fb020f068f5223"
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
